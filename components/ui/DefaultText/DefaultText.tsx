import React from 'react';
import { Text } from 'react-native';

import styles from './defaultTextStyle';

const DefaultText: React.FC = ({ children }) => children ? <Text style={styles.text}>{children}</Text> : null;

export default DefaultText;
