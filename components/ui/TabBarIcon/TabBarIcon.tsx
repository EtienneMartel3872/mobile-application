import React from 'react';
import { Ionicons } from '@expo/vector-icons';

import styles from './tabBarIconStyle';

interface TabBarIconProps {
  name: string;
}

/** Icons for the navigator */
const TabBarIcon: React.FC<TabBarIconProps> = ({ name }) => (
  <Ionicons
    name={name}
    size={26}
    style={styles.icon}
  />
);

export default TabBarIcon;
