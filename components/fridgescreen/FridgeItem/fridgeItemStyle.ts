import { StyleSheet } from 'react-native';

import Colors from '../../../constants/colors';

export default StyleSheet.create({
    deleteIcon: {
        color: Colors.deleteRed,
        fontSize: 40,
        marginBottom: 10,
    },
});