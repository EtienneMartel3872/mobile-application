import React from 'react';
import {
  Text,
  View,
} from 'react-native';

import Icon from '../../ui/DynamicIcon/DynamicIcon';
import { FridgeItemType } from '../../../constants/types';
import styles from './fridgeItemStyle';

interface FridgeItemProps extends FridgeItemType {
  deleteFunction: (id: number) => void;
}

/** TODO: STYLING + THUMBNAIL?  */
const FridgeItem: React.FC<FridgeItemProps> = ({
  id, type, name, date, status, deleteFunction
}) => {
  return (
    <View>
      <Text>This is item {id}</Text>
      <Icon
        name="trash"
        type="EvilIcons"
        iconStyle={styles.deleteIcon}
        onPressHandler={() => deleteFunction(id)}
      />
    </View>
  );
};

export default FridgeItem;
