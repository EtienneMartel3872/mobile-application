import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  notificationList: {
    maxHeight: 500,
    position: 'absolute',
    right: 0,
    top: 50,
    width: 60,
  },
});