import React from 'react';
import { FlatList } from 'react-native';

import NotificationItem from '../NotificationItem/NotificationItem';
import { Notification } from '../../../../constants/types';
import styles from './notificationPanelStyle';

interface NotificationPanelProps {
  notifications: Notification[];
}

/** The container of each notification to display on the main screen */
const NotificationPanel: React.FC<NotificationPanelProps> = ({ notifications }) => {
  /** Sorts the notifications before displaying them
   *  Higher severity/oldest first. 1 is the highest priority 
   */
  const sortedNotifications = notifications.sort((a, b) => {
    if (a.severity < b.severity) return -1;
    if (a.sentDate < b.sentDate && a.severity === b.severity) return -1;
    return 0;
  });

  // Render flatlist  of notification items (list with not everything rendered and key-value pairs)
  return (
    <FlatList
      style={styles.notificationList}
      data={sortedNotifications}
      keyExtractor={(n) => (`${n.id}`)}
      renderItem={({ item }) => (
        <NotificationItem
          id={item.id}
          title={item.title}
          type={item.type}
          description={item.body}
          isDeletable={item.isDeletable}
        />
      )}
    />
  );
};

export default NotificationPanel;
