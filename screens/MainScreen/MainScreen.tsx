import React, { useState, useEffect } from 'react';
import {
  Text,
  View,
  TouchableWithoutFeedback,
  AsyncStorage,
} from 'react-native';
import { NavigationEvents, NavigationParams } from 'react-navigation';
import  * as Permissions from 'expo-permissions';
import { AntDesign } from '@expo/vector-icons';

import NotificationPanel from '../../components/mainscreen/notification/NotificationPanel/NotificationPanel';
import ConfirmationModal from '../../components/mainscreen/ConfirmationModal/ConfirmationModal';
import MainCamera from '../../components/mainscreen/MainCamera/MainCamera';
import { NOTIFICATIONS } from '../../constants/variables';
import styles from './mainScreenStyle';

const MainScreen: React.FC<NavigationParams> = ({ navigation }) => {
  // STATES AND VARIABLES //
  const [hasCameraPermission, setHasCameraPermission] = useState<boolean>(false);
  const [isLoaded, setIsLoaded] = useState<boolean>(true);
  const [pickedImage, setPickedImage] = useState<string | undefined>();
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);

  // CHECKS AND PREPARATION //
  // Check if signed in
  const fetchAuth = async (): Promise<void> => {
    try {
      const value = await AsyncStorage.getItem('guestMode');
      if (value !== null) {
        return;
      }
    } catch (error) {
      // console.log('error fetching guest mode');
    }
    navigation.navigate('Sign in', {});
  };

  // Check and sets if permissions are enabled, will ask for permission if it was never asked before
  const checkOnMount = async (): Promise<void> => {
    const response = await Permissions.askAsync(Permissions.CAMERA);
    setHasCameraPermission(response.status === 'granted');
  };

  // Once the component is mounted, check permissions and status
  useEffect(() => {
    (async() => {
      await fetchAuth();
      await checkOnMount();
    })();
  }, [fetchAuth, checkOnMount]);

  // RENDERING //
  const notifs = (): JSX.Element => (
    <NotificationPanel notifications={NOTIFICATIONS} />
  );

  const tabButton = (): JSX.Element => (
    <TouchableWithoutFeedback onPress={() => { navigation.toggleDrawer(); }}>
      <View style={styles.icon}>
        <AntDesign name="menuunfold" size={15} />
      </View>
    </TouchableWithoutFeedback>
  );

  const renderNoPermissions = (text: string): JSX.Element => (
    <View style={styles.content}>
      {notifs()}
      <Text>{text}</Text>
      {tabButton()}
    </View>
  );

  // Final render if all goes right
  const renderBody = (): JSX.Element => (
    <View style={styles.globalContainer}>
      <View style={styles.content}>
        <NavigationEvents onWillFocus={() => setIsLoaded(true)} onDidBlur={() => setIsLoaded(false)} />
        <ConfirmationModal
          pickedImage={pickedImage || ''}
          isModalVisible={isModalVisible}
          setIsModalVisible={setIsModalVisible}
          navigation={navigation}
        />
        <View style={styles.cameraContainer}>
          {isLoaded && (
            <MainCamera setIsModalVisible={setIsModalVisible} setPickedImage={setPickedImage} />
          )}
        </View>
      </View>
      {tabButton()}
      {notifs()}
    </View>
  );

  let cameraScreenContent: JSX.Element;
  if (hasCameraPermission === null) {
    cameraScreenContent = renderNoPermissions('Waiting for permissions');
  } else if (!hasCameraPermission) {
    cameraScreenContent = renderNoPermissions(
      'No access to camera. Enable camera permissions in our phone&apos;s settings',
    );
  } else {
    cameraScreenContent = renderBody();
  }

  return (
    <View style={styles.container}>
      {cameraScreenContent}
    </View>
  );
};

export default MainScreen;
