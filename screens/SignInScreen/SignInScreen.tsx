import React from 'react';
import {
  View, Button, AsyncStorage, Alert
} from 'react-native';
import { NavigationParams } from 'react-navigation';

import styles from './signInScreenStyle';

// TODO: replace buttons for the ones that will be used when accounts are made

/** Renders the sign in screen. Currently only guest is allowed */
const SignInScreen: React.FC<NavigationParams> = ({ navigation }) => {
  /** Stores the current mode data (logged in or guest) */
  const storeGuest = async (isOn: string) => {
    try {
      await AsyncStorage.setItem('guestMode', isOn);
    } catch (error) {
      Alert.alert(
        'Error',
        'Error occured saving guest mode',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
      );
    }
  };

  return (
    <View style={styles.content}>
      <View style={styles.buttons}>
        <Button
          title="Login with Facebook"
          disabled
          onPress={async () => {
            Alert.alert(
              'Unimplemented',
              'Accounts are not enabled as of right now',
              [{ text: 'OK' }],
            );
            await storeGuest('false');
          }}
        />
        <Button
          title="Login with Google"
          disabled
          onPress={() => {
            Alert.alert(
              'Unimplemented',
              'Accounts are not enabled as of right now',
              [{ text: 'OK' }],
            );
            storeGuest('false');
          }}
        />
        <Button
          title="Continue as guest"
          onPress={async () => {
            await storeGuest('true');
            navigation.navigate('Fruit Scanner');
          }}
        />
      </View>
    </View>
  );
};

export default SignInScreen;
