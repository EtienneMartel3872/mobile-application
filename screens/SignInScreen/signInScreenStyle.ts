import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    buttons: {
        height: 150,
        justifyContent: 'space-between',
        marginTop: 20,
        width: 200,
    },
    content: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
    },
});
