import React from 'react';
import {
  View,
  Text,
  Button,
  Image,
  Alert
} from 'react-native';
import { NavigationParams } from 'react-navigation';

import { getDetails, ResultsProps } from './pictureDetailsScreen.util';
import styles from './pictureDetailsScreenStyle';

/** Renders the result screen once the data was received */
const PictureDetailsScreen: React.FC<NavigationParams> = ({ navigation }) => {
  const results: ResultsProps = navigation.getParam('results');
  const image: string = navigation.getParam('image');

  const {
    title, accuracy, freshness, isSaveable, fruitColor, accuracyColor, freshnessColor
  } = getDetails(results);

  return (
    <View style={styles.content}>
      <View style={styles.titleContainer}>
        <Text style={{ ...styles.title, color: fruitColor }}>
          {title}
        </Text>
      </View>
      <View style={styles.imagePreview}>
        <Image
          style={styles.image}
          source={{ uri: image }}
        />
      </View>
      <View style={styles.textContainer}>
        <Text style={{ ...styles.text, color: freshnessColor }}>
          {freshness}
        </Text>
        <Text style={{ ...styles.text, color: accuracyColor }}>
          {accuracy}
        </Text>
      </View>
      <View style={styles.actionContainer}>
        <View style={styles.action}>
          <Button title="Back" onPress={() => navigation.goBack()} />
        </View>
        <View style={styles.action}>
          <Button
            title="Save"
            disabled={!isSaveable}
            onPress={() => {
              Alert.alert(
                'Unimplemented',
                'Accounts are not enabled as of right now, the save operation will be enabled in a future version',
                [{ text: 'OK' }],
              );
            }}
          />
        </View>
      </View>
    </View>
  );
};

export default PictureDetailsScreen;
