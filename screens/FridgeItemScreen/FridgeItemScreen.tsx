import React from 'react';
import {
  View,
  Text,
} from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { NavigationParams } from 'react-navigation';
import { NavigationStackScreenComponent } from 'react-navigation-stack';

import HeaderButton from '../../components/ui/HeaderButton/HeaderButton';
import styles from './fridgeItemScreenStyle';

//TODO: display item data, image, create reminder feature, style

/** Renders the page with all the fridge options */
const FridgeItemScreen: NavigationStackScreenComponent = ({ navigation }) => {
  const item = navigation.getParam('item');
  return (
    <View style={styles.content}>
      <Text>Selected Item: {item.id}</Text>
    </View>
  );
};

FridgeItemScreen.navigationOptions = (navData: NavigationParams) => ({
  headerTitle: 'Fridge Item',
  headerLeft: (
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
      <Item
        title="Menu"
        iconName="ios-menu"
        onPress={() => {
          navData.navigation.toggleDrawer();
        }}
      />
    </HeaderButtons>
  ),
});

export default FridgeItemScreen;
