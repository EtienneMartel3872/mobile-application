import React, { useState, useEffect } from 'react';
import {
  View,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { NavigationParams } from 'react-navigation';
import { NavigationStackScreenComponent } from 'react-navigation-stack';

import HeaderButton from '../../components/ui/HeaderButton/HeaderButton';
import FridgeItem from '../../components/fridgescreen/FridgeItem/FridgeItem';
import { FridgeItemType } from '../../constants/types';
import { fetchFridge } from './fridgeScreen.util';
import styles from './fridgeScreenStyle';

// TODO: invite the user to register/sign in when they reach this page

/** Renders the page with all the fridge options */
const FridgeScreen: NavigationStackScreenComponent = ({ navigation }) => {
  const [isFetched, setIsFetched] = useState<boolean | undefined>(false);
  const [fridgeItems, setFridgeItems] = useState<FridgeItemType[] | undefined>();

  /** TODO:  DELETE FROM DB WHEN ACCOUNTS ARE CONNECTED, NOT JUST LOCALLY + ALSO ADD CONFIRMATION*/
  const FridgeRemoveItem = (id: number) => {
    if (fridgeItems) {
      setFridgeItems(fridgeItems.filter(i => i.id != id));
    }
  }

  useEffect(() => {
    let isSubscribed = true;
    (async () => {
      if (!isFetched) {
        const fridge: FridgeItemType[] = await fetchFridge();
        if (isSubscribed) {
          setFridgeItems(fridge);
          setIsFetched(true);
        }
      }
    })();

    return () => {
      isSubscribed = false;
    };
  }, []);
  
  return (
    <View style={styles.content}>
      <FlatList
        data={fridgeItems}
        keyExtractor={(n) => (`${n.id}`)}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={() => navigation.navigate('FridgeItem', { item })}
          >
            <FridgeItem
              id={item.id}
              name={item.name}
              type={item.type}
              date={item.date}
              status={item.status}
              deleteFunction={FridgeRemoveItem}
            />
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

FridgeScreen.navigationOptions = (navData: NavigationParams) => ({
  headerTitle: 'Meal Categories',
  headerLeft: (
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
      <Item
        title="Menu"
        iconName="ios-menu"
        onPress={() => {
          navData.navigation.toggleDrawer();
        }}
      />
    </HeaderButtons>
  ),
});

export default FridgeScreen;
