import { FridgeItemType } from "../../constants/types";

/** TODO: REPLACE WITH ACTUAL FETCH FROM DB WHEN ACCOUNTS ARE MADE */
export const fetchFridge = (): Promise<FridgeItemType[]> => {
  return Promise.resolve([
    { id: 1, type: "scan", name: "apple", date: Date.now(), status: "98%" }, 
    { id: 2, type: "scan", name: "apple", date: Date.now(), status: "98%" }, 
    { id: 3, type: "scan", name: "apple", date: Date.now(), status: "98%" }, 
    { id: 4, type: "scan", name: "apple", date: Date.now(), status: "98%" }, 
    { id: 5, type: "scan", name: "apple", date: Date.now(), status: "98%" }, 
    { id: 6, type: "scan", name: "apple", date: Date.now(), status: "98%" },
    { id: 7, type: "scan", name: "apple", date: Date.now(), status: "98%" }
  ]);
}