import React from 'react';
import {
  View,
  Button,
  AsyncStorage,
  Alert,
} from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { NavigationParams } from 'react-navigation';
import { NavigationStackScreenComponent } from 'react-navigation-stack';

import HeaderButton from '../../components/ui/HeaderButton/HeaderButton';
import styles from './settingsScreenStyle';

/** Renders the page with all the setting options */
const SettingsScreen: NavigationStackScreenComponent = () => (
  <View style={styles.content}>
    <Button
      title="Clear cache"
      onPress={() => {
        AsyncStorage.removeItem('guestMode');
        AsyncStorage.removeItem('deletedNotifications');
        Alert.alert('Cache cleared', 'Your cache has been cleared', [{ text: 'OK' }]);
      }}
    />
  </View>
);

SettingsScreen.navigationOptions = (navData: NavigationParams) => ({
  headerLeft: (
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
      <Item
        title="Menu"
        iconName="ios-menu"
        onPress={() => {
          navData.navigation.toggleDrawer();
        }}
      />
    </HeaderButtons>
  ),
});

export default SettingsScreen;
