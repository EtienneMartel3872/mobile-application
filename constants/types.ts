export interface Notification {
    id: number;
    title: string;
    body: string;
    type: string;
    severity: number;
    sentDate: number;
    expirationDate: number;
    isDeletable: boolean;
    scope: string | null;
}

export interface FridgeItemType {
    id: number;
    type: string;
    name: string;
    date: number;
    status: string;
}