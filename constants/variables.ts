import { Notification } from "./types";

const n1: Notification = {
  id: 1,
  title: 'Versions',
  body: 'Please make sure to check on GitLab regularly for new versions as we implement more features.',
  type: 'Warning',
  severity: 2,
  sentDate: 1,
  expirationDate: 1,
  isDeletable: false,
  scope: null
};

const n2: Notification = {
  id: 2,
  title: 'Welcome!',
  body: 'Welcome to IFMAP!\nFor the best experience, please scan pictures by using the photo button when your fruit is centered in the white box and resting on a flat surface.\nThank you for being here.',
  type: 'Info',
  severity: 1,
  sentDate: 1,
  expirationDate: 1,
  isDeletable: true,
  scope: null
};

const n3: Notification = {
  id: 3,
  title: 'New Features',
  body: 'New features: You can now change the flash of your camera or upload your own pictures from your camera roll.\nWe have also improved the display of the main screen.',
  type: 'Info',
  severity: 2,
  sentDate: 1,
  expirationDate: 1,
  isDeletable: true,
  scope: null
};

export const NOTIFICATIONS = [n1, n2, n3];
