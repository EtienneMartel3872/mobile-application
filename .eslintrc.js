module.exports = {
    parser: "@typescript-eslint/parser",
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: "module"
    },
    extends: [
        "plugin:react/recommended",
        "plugin:react-native/all",
        "plugin:@typescript-eslint/recommended"
      ],
    settings: {
        react: {
            version: "detect"
        }
    },
    rules: {
        "react/prop-types": 'off',
        "react/display-name": 'off'
    }
};
