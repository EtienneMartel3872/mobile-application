import React from 'react';
import {
  Platform,
  View,
  Text,
  SafeAreaView
} from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator, NavigationStackOptions } from 'react-navigation-stack';
import { createDrawerNavigator, DrawerNavigatorItems } from 'react-navigation-drawer';
import { DrawerNavigatorItemsProps, NavigationDrawerOptions } from 'react-navigation-drawer/lib/typescript/src/types';

import TabBarIcon from '../components/ui/TabBarIcon/TabBarIcon';
import SignInScreen from '../screens/SignInScreen/SignInScreen';
import MainScreen from '../screens/MainScreen/MainScreen';
import PictureDetailsScreen from '../screens/PictureDetailsScreen/PictureDetailsScreen';
import SettingsScreen from '../screens/SettingsScreen/SettingsScreen';
import FridgeScreen from '../screens/FridgeScreen/FridgeScreen';
import FridgeItemScreen from '../screens/FridgeItemScreen/FridgeItemScreen';
import Colors from '../constants/colors';
import styles from './mainNavigatorStyle';

/** Renders the navigator with various screen stacks, also where the menu header is customized */
const customDrawerNavigation: React.FC<DrawerNavigatorItemsProps> = (props) => (
  <View style={styles.drawer}>
    <SafeAreaView>
      <View style={styles.labelContainer}>
        <Text style={styles.labelHeader}>
          IFMAP - LRIMa
        </Text>
        <Text style={styles.labelBody}>
          Jihene Rezgui, Thomas Trépanier, David Génois, Hervé Claden and Amasten Ameziani
        </Text>
      </View>
      <DrawerNavigatorItems {...props} />
    </SafeAreaView>
  </View>
);

/** Default navigation options */
const defaultStackNavigationOptions = {
  headerStyle: {
    backgroundColor: Platform.OS === 'android' ? Colors.primary : '',
  },
  headerTitleStyle: {
    fontFamily: 'open-sans-bold',
  },
  headerBackTitleStyle: {
    fontFamily: 'open-sans',
  },
  headerTintColor: Platform.OS === 'android' ? 'white' : Colors.primary,
  headerTitle: 'Default Title',
} as NavigationStackOptions;

const defaultDrawerOptions = {
  defaultNavigationOptions: defaultStackNavigationOptions,
  drawerType: "slide",
  minSwipeDistance: 500,
  title: 'test',
  contentComponent: customDrawerNavigation,
} as NavigationDrawerOptions

/** Login stack */
const AuthStack = createStackNavigator({
  SignIn: {
    screen: SignInScreen,
    navigationOptions: {
      headerTitle: 'Welcome!',
    },
  },
}, {
  defaultNavigationOptions: defaultStackNavigationOptions,
  navigationOptions: {
    drawerIcon: () => (
      <TabBarIcon
        name={Platform.OS === 'ios' ? 'ios-log-in' : 'md-log-in'}
      />
    ),
  },
});

/** Mainscreen Stack */
const MainStack = createStackNavigator({
  Main: {
    screen: MainScreen,
    navigationOptions: {
      header: null,
    },
  },
  PictureDetails: {
    screen: PictureDetailsScreen,
    navigationOptions: {
      headerTitle: 'Analysis Results',
    },
  },
}, {
  defaultNavigationOptions: defaultStackNavigationOptions,
  navigationOptions: {
    drawerIcon: () => (
      <TabBarIcon
        name={Platform.OS === 'ios' ? 'ios-qr-scanner' : 'md-qr-scanner'}
      />
    ),
  },
});

/** Setting Stack */
const SettingsStack = createStackNavigator({
  Settings: {
    screen: SettingsScreen,
    navigationOptions: {
      headerTitle: 'Settings',
    },
  },
}, {
  defaultNavigationOptions: defaultStackNavigationOptions,
  navigationOptions: {
    drawerIcon: () => (
      <TabBarIcon
        name={Platform.OS === 'ios' ? 'ios-settings' : 'md-settings'}
      />
    ),
  },
});

/** Virtual Fridge Stack */
const FridgeStack = createStackNavigator({
  Fridge: {
    screen: FridgeScreen,
    navigationOptions: {
      headerTitle: 'Virtual Fridge',
    },
  },
  FridgeItem: {
    screen: FridgeItemScreen,
    navigationOptions: {
      headerTitle: 'Virtual Fridge Item',
    },
  },
}, {
  defaultNavigationOptions: defaultStackNavigationOptions,
  navigationOptions: {
    drawerIcon: () => (
      <TabBarIcon
        name={Platform.OS === 'ios' ? 'ios-basket' : 'md-basket'}
      />
    ),
  },
});

/** The drawer with the stacks (menu) */
const MainDrawer = createDrawerNavigator({
  'Fruit Scanner': MainStack,
  'Virtual Fridge': FridgeStack,
  'Sign in': {
    screen: AuthStack,
    navigationOptions: {
      drawerLockMode: 'locked-closed',
    },
  },
  'App Settings': SettingsStack,
}, defaultDrawerOptions);

const App = createSwitchNavigator({
  App: MainDrawer,
}, {
  defaultNavigationOptions: defaultStackNavigationOptions,
});

export default createAppContainer(App);
